package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.TeleOpDrive.ControllerDrive;

@TeleOp(name="DriverControlledOpMode", group="Linear Opmode")
public class DriverControlledOpMode extends LinearOpMode {

    private ElapsedTime runtime = new ElapsedTime();

    private DcMotor frontRight;
    private DcMotor frontLeft;
    private DcMotor backRight;
    private DcMotor backLeft;


    @Override
    public void runOpMode() {
        telemetry.addData("Status", "Initialized");
        telemetry.update();

        frontRight = hardwareMap.dcMotor.get("frontRight");
        frontLeft = hardwareMap.dcMotor.get("frontLeft");
        backRight = hardwareMap.dcMotor.get("backRight");
        backLeft = hardwareMap.dcMotor.get("backLeft");

        waitForStart();
        runtime.reset();

        while (opModeIsActive()) {

            ControllerDrive.drive(frontRight,
                    frontLeft,
                    backRight,
                    backLeft,
                    gamepad1
                    );

            telemetry.addData("Time", runtime.toString());
            telemetry.update();
        }
    }
}
