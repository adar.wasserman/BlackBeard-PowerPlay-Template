package org.firstinspires.ftc.teamcode.TeleOpDrive;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Gamepad;

public class ControllerDrive {

    public static void drive(DcMotor frontRight, DcMotor frontLeft, DcMotor backRight, DcMotor backLeft, Gamepad driverController){
        double leftStickY = -driverController.left_stick_y; // Remember, this is reversed!
        double leftStickX = driverController.left_stick_x * 1.1; // Counteract imperfect strafing
        double rightStickX = driverController.right_stick_x;

        double denominator = Math.max(Math.abs(leftStickY) + Math.abs(leftStickX) + Math.abs(rightStickX), 1);
        double frontLeftPower = (leftStickY + leftStickX + rightStickX) / denominator;
        double backLeftPower = (leftStickY - leftStickX + rightStickX) / denominator;
        double frontRightPower = (leftStickY - leftStickX - rightStickX) / denominator;
        double backRightPower = (leftStickY + leftStickX - rightStickX) / denominator;

        frontLeft.setPower(frontLeftPower);
        backLeft.setPower(backLeftPower);
        frontRight.setPower(frontRightPower);
        backRight.setPower(backRightPower);
    }

}
